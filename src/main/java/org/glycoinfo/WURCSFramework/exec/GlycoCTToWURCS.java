package org.glycoinfo.WURCSFramework.exec;

import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSExporterGlycoCT;

public class GlycoCTToWURCS {

	// Version
	private static final String VERSION = "2.0.160907";

	public static void main(String[] args) {
		String t_strCode = "";
/*
		t_strCode = "RES\n"
				+"1b:x-HEX-x:x\n"
				+"2s:n-acetyl\n"
				+"3b:x-HEX-x:x\n"
				+"4s:n-acetyl\n"
				+"5b:x-HEX-x:x\n"
				+"6s:n-acetyl\n"
				+"7b:x-HEX-x:x\n"
				+"8s:n-acetyl\n"
				+"9b:x-HEX-1:5\n"
				+"10b:x-HEX-1:5\n"
				+"11b:x-HEX-1:5\n"
				+"12b:x-HEX-1:5\n"
				+"13b:x-HEX-1:5\n"
				+"14b:x-HEX-1:5|0:d\n"
				+"LIN\n"
				+"1:1d(2+1)2n\n"
				+"2:3d(2+1)4n\n"
				+"3:5d(2+1)6n\n"
				+"4:7d(2+1)8n\n";
*/
		for ( int i = 0; i < args.length; i++ ) {

			// For usage
			if ( args[i].equals("-help") ) {
				usage();
				System.exit(0);
			}

			// Replace space " " to linefeed code "\n"
			String[] arg = args[i].split(" ");
			if ( arg.length == 1 ) {
				t_strCode += args[i] + "\n";
				continue;
			}
			for ( String str : arg) {
				t_strCode += str + "\n";
			}
		}

		start(t_strCode);
	}

	public static void start(String a_strGlycoCT) {
		WURCSExporterGlycoCT t_oExporter = new WURCSExporterGlycoCT();
		try {
			t_oExporter.start(a_strGlycoCT);
			String t_strWURCS = t_oExporter.getWURCS();
			System.out.println(t_strWURCS);

		} catch ( SugarImporterException e ) {
			String message = "There is an error in importer of GlycoCT.\n";
			if ( e.getErrorText() != null ) {
				message += e.getErrorText();
			}
			System.out.println(message);
			e.printStackTrace();
		} catch ( GlycoVisitorException e ) {
			String message = "There is an error in GlycoCT validation.\n";
			message += e.getErrorMessage();

			String t_strLog = t_oExporter.getValidationErrorLog().toString();
			if ( !t_strLog.equals("") )
				message += t_strLog;

			System.out.println(message);
			e.printStackTrace();
		} catch (WURCSException e) {
			System.out.println(e.getErrorMessage());
			e.printStackTrace();
		}
	}

	private static void usage() {
		System.err.println("WURCS2.0 Conversion System from GlycoCT");
		System.err.println("\tCurrent version: "+VERSION);
		System.err.println();
		System.err.println("Usage: java (this program).jar [OPTION]... [GlycoCT]... ");
		System.err.println();
		System.err.println("where OPTION include:");
		System.err.println("\t-help\t\tto print this help message");
		System.err.println();
		System.err.println("A space \" \" is also usable as a separator to separate each line of GlycoCT instead of linefeed code.");
	}
}
