package org.glycoinfo.WURCSFramework.exec;

import java.io.File;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.TreeSet;

import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.glycoinfo.WURCSFramework.util.GlycoCTListReader;
import org.glycoinfo.WURCSFramework.util.WURCSConversionLogger;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSExporterGlycoCT;
import org.glycoinfo.WURCSFramework.util.WURCSExporterGlycoCTList;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.WURCSFileWriter;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSGraphExporterUniqueMonosaccharides;

public class GlycoCTToWURCSFromFile extends GlycoCTToWURCS {

	// Version
	private static final String VERSION = "2.0.160907";

	public static void main(String[] args) {
		LinkedList<String> t_aFilenames = new LinkedList<String>();
		for ( int i = 0; i < args.length; i++ ) {

			// For usage
			if ( args[i].equals("-help") ) {
				usage();
				System.exit(0);
			}
			// For collect monosaccharides (ResidueCodes)
			boolean t_bResidueCodeCollection = false;
			if ( args[i].equals("-MS") )
				t_bResidueCodeCollection = true;

			// Check filepath
			String t_strFilePath = args[i];
			File t_oFile = new File(t_strFilePath);
			if ( !t_oFile.exists() ) {
				System.err.println("The file not exists: " + t_strFilePath);
				continue;
			}
			t_aFilenames.addLast(t_strFilePath);
			// Make result dir
			String t_strResultDir = t_oFile.getAbsolutePath() + "_result";


			// Read GlycoCT list
			System.err.println("Read GlycoCT list from: "+t_strFilePath);
			TreeMap<String, String> t_mapIDToGlycoCT = null;
			try {
				if ( t_oFile.isFile() )
					t_mapIDToGlycoCT = GlycoCTListReader.readFromFile(t_strFilePath);
				if ( t_oFile.isDirectory() )
					t_mapIDToGlycoCT = GlycoCTListReader.readFromDir(t_strFilePath);
			} catch (Exception e) {
				System.err.println("The file can not read: " + t_strFilePath);
				e.printStackTrace();
				continue;
			}

			// Convert
			WURCSExporterGlycoCTList t_oExport = new WURCSExporterGlycoCTList();
			t_oExport.start(t_mapIDToGlycoCT);

			// Print result
			WURCSFileWriter.printWURCSList(t_oExport.getMapIDToWURCS(), t_strResultDir, "WURCSList.txt");

			// Print result
//			WURCSFileWriter.printWURCSList(t_mapIDToWURCS, t_strFilePathResult, t_strFileNameW);
//			WURCSFileWriter.printMSList(t_setUniqueMS, t_strFilePathResult, t_strFileNameMS);
//			t_oLogger.setInputFileName(t_strFileNameR);
//			t_oLogger.printLog( WURCSFileWriter.getResultFilePath(t_strFilePathResult, t_strFileNameLog) );
//			System.out.println("Output result to "+t_strFilePathResult);
		}

	}

	/** Convert and output WURCS from GlycoCT list */
	public static TreeMap<String, String> GlycoCTListToWURCS(TreeMap<String, String> a_mapGlycoCTList, TreeSet<String> a_setUniqueMS, WURCSConversionLogger a_oLogger) {

		TreeMap<String, String> t_mapIDToWURCS = new TreeMap<String, String>();

		for ( String ID : a_mapGlycoCTList.keySet() ) {

			String t_strCode = a_mapGlycoCTList.get(ID);
//			System.out.println(t_strCode);
			String t_strWURCS = "";
			String t_strMessage = "";
			WURCSExporterGlycoCT t_oExporter = new WURCSExporterGlycoCT();
//			StringBuffer t_strValidateGlycoCT = new StringBuffer("");
			try {
				// Start conversion
				/*
				SugarImporter t_oImporterGlycoCT = new SugarImporterGlycoCTCondensed();
				Sugar gx = t_oImporterGlycoCT.parse(t_strCode);
				gx = normalizeSugar(gx, t_strValidateGlycoCT);

				SugarToWURCSGraph t_oS2G = new SugarToWURCSGraph();
				t_oS2G.start(gx);
				WURCSGraph t_oGraph = t_oS2G.getGraph();

				// Get WURCS string
				WURCSFactory t_oFactory = new WURCSFactory(t_oGraph);
				t_strWURCS = t_oFactory.getWURCS();
				*/
				System.out.println(ID);
				t_oExporter.start(t_strCode);
				t_strWURCS = t_oExporter.getWURCS();

//				System.out.println( ID + "\t" + t_strWURCS );
//				String t_strWURCS = m_objSugarToWURCSGraph.getWURCSCompress();
				if( t_strWURCS.contains("WURCS") ) {
					// Collect unique monosaccharide
					WURCSGraphExporterUniqueMonosaccharides t_oExportMS = new WURCSGraphExporterUniqueMonosaccharides();
					WURCSFactory t_oFactory = new WURCSFactory(t_strWURCS);
					t_oExportMS.start( t_oFactory.getGraph() );
					for ( String t_strUniqueMS : t_oExportMS.getMSStrings() ) {
						if ( a_setUniqueMS.contains(t_strUniqueMS) ) continue;
						a_setUniqueMS.add(t_strUniqueMS);
					}
				}
//				System.out.println(ID);

			} catch ( SugarImporterException e ) {
				t_strMessage = "There is an error in GlycoCT importer";
				if ( e.getErrorText() != null ) {
					t_strMessage = e.getErrorText();
				}
			} catch ( GlycoVisitorException e ) {
				t_strMessage = e.getErrorMessage();
			} catch (WURCSException e) {
				t_strMessage = e.getErrorMessage();
			}

			if( t_strWURCS.contains("WURCS") ) {
				// Set result
				t_mapIDToWURCS.put(ID, t_strWURCS);
				a_oLogger.addWURCS(ID, t_strWURCS);
//				continue;
			}
			a_oLogger.addMessage(ID, t_strMessage, t_oExporter.getValidationErrorLog().toString());
		}

		return t_mapIDToWURCS;
	}

	private static void usage() {
		System.err.println("WURCS2.0 Conversion System from GlycoCT");
		System.err.println("\tCurrent version: "+VERSION);
		System.err.println();
		System.err.println("Usage: java (this program).jar [OPTION]... [GlycoCT]... ");
		System.err.println();
		System.err.println("where OPTION include:");
		System.err.println("\t-help\t\tto print this help message");
		System.err.println();
		System.err.println("A space \" \" is also usable as a separator to separate each line of GlycoCT instead of linefeed code.");
	}

}
