package org.glycoinfo.WURCSFramework.exec;

import java.util.TreeMap;

import org.glycoinfo.WURCSFramework.util.GlycoCTListReader;

public class GlycoCTToWURCSFromFileByID extends GlycoCTToWURCS {

	public static void main(String[] args) {

		String t_strFileNameR = "";
		// for GlycomeDB
//		t_strFileNameR = "glycomedb_structures-02_06_2015-02_57_convert.txt";
		// for GlyTouCan
//		t_strFileNameR = "glytoucanAccGlycoCTcsvToText_20150220.txt";
		t_strFileNameR = "glytoucanAccGlycoCTcsvToText_20150717.txt";
		String t_strGCTList_GlyTouCan =
				GlycoCTToWURCSFromFileByID.class.getClassLoader().getResource(t_strFileNameR).getPath();

		// Read GlycoCT list from file
		TreeMap<String,String> t_mapGlycoCTList = new TreeMap<String,String>();
		try {
			t_mapGlycoCTList = GlycoCTListReader.readFromFile(t_strGCTList_GlyTouCan);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		// Get GlycoCT by ID
		String ID = "G44067OU";
		String t_strCode = t_mapGlycoCTList.get(ID);

		start(t_strCode);
		/*
		StringBuffer t_strValidateGlycoCT = new StringBuffer("");
		try {
			System.out.println(t_strCode);

			// Start conversion
			SugarImporter t_oImporterGlycoCT = new SugarImporterGlycoCTCondensed();
			Sugar gx = t_oImporterGlycoCT.parse(t_strCode);
			gx = normalizeSugar(gx, t_strValidateGlycoCT);
			if ( !t_strValidateGlycoCT.toString().equals("") )
				System.out.println(t_strValidateGlycoCT.toString());
			SugarToWURCSGraph t_oS2G = new SugarToWURCSGraph();
			t_oS2G.start(gx);
			WURCSGraph t_oGraph = t_oS2G.getGraph();
			// Get WURCS string
			WURCSFactory t_oFactory = new WURCSFactory(t_oGraph);
			String t_strWURCS = t_oFactory.getWURCS();

//			String t_strWURCSCompress = t_objExporterWURCS.getWURCSCompressWithRepeat();
			System.out.println( ID + "\t" + t_strWURCS );
			System.out.println( "Length: " + t_strWURCS.length() );
//			System.out.println( ID + "\t" + t_strWURCSCompress );
//			System.out.println( "Compressed: " + t_strWURCSCompress.length() );

			String t_strWURCSURL = URLEncoder.encode(t_strWURCS, "UTF-8");
			System.out.println( ID + "\t" + t_strWURCSURL );
			System.out.println( "URLLength: " + t_strWURCSURL.length() );
//			String t_strWURCSCompressURL = URLEncoder.encode(t_strWURCSCompress, "UTF-8");
//			System.out.println( ID + "\t" + t_strWURCSCompressURL );
//			System.out.println( "Compressed: " + t_strWURCSCompressURL.length() );

		} catch ( SugarImporterException e ) {
			String message = "There is an error in GlycoCT";
			if ( e.getErrorText() != null ) {
				message = e.getErrorText();
			}
			System.out.println(message);
			System.out.println( ID + "\t" + message);
		} catch ( GlycoVisitorException e ) {
			System.out.println(e.getErrorMessage());
			if ( !t_strValidateGlycoCT.toString().equals("") )
				System.out.println(t_strValidateGlycoCT.toString());
			System.out.println( ID + "\t" + e.getErrorMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (WURCSException e) {
			e.printStackTrace();
		}
		*/

	}

}
