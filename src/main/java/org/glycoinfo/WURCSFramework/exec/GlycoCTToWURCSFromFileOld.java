package org.glycoinfo.WURCSFramework.exec;

import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.glycoinfo.WURCSFramework.util.GlycoCTListReader;
import org.glycoinfo.WURCSFramework.util.WURCSConversionLogger;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSExporterGlycoCT;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.WURCSFileWriter;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSGraphExporterUniqueMonosaccharides;

public class GlycoCTToWURCSFromFileOld extends GlycoCTToWURCS {

	public static void main(String[] args) {

		HashMap<String, String> t_mapFileNameRToW = new HashMap<String, String>();
		// For GlycoEpitope
//		t_mapFileNameRToW.put("Glycoepitope_GlycoCTList_20150224.txt",             "result-Glycoepitope");
		// For GlyTouCan Motif
//		t_mapFileNameRToW.put("GlyTouCan_GlycoCTListMotif_20150213.txt",           "result-GlyTouCanMotif");
		// For GlyTouCan
//		t_mapFileNameRToW.put("glytoucanAccGlycoCTcsvToText_20150220.txt",         "result-GlyTouCan");
		t_mapFileNameRToW.put("glytoucanAccGlycoCTcsvToText_20150717.txt",         "result-GlyTouCan");
		// For GlycomeDB
//		t_mapFileNameRToW.put("GlycomeDB_GlycoCTList_uniqueID_20140613112537.txt", "result-GlycomeDB");
		t_mapFileNameRToW.put("glycomedb_structures-02_06_2015-02_57_convert.txt", "result-GlycomeDB");
		// For UnicarbKB
		t_mapFileNameRToW.put("20160616_UnicarbKB_Id-glycoct.txt",                 "result-UnicarbKB");

		// Get ClassLoader for input resouces and output result
		ClassLoader t_oCL = GlycoCTToWURCSFromFileOld.class.getClassLoader();
		// Get path for output result
		String t_strFilePathResult = t_oCL.getResource("result/").getPath();
		try {
			for ( String t_strFileName : t_mapFileNameRToW.keySet() ) {
//				String t_strFileNameR   = t_strFilePathRoot+t_strFileName;
				// Get path for input GlycoCT list file
				String t_strFileNameR   =t_oCL.getResource(t_strFileName).getPath();
				String t_strFileNameW   = t_mapFileNameRToW.get(t_strFileName)+"_WURCSList.txt";
				String t_strFileNameMS  = t_mapFileNameRToW.get(t_strFileName)+"_MSList.txt";
				String t_strFileNameLog = t_mapFileNameRToW.get(t_strFileName)+".log";
//				PrintWriter t_pwWURCSList = FileIOUtils.openTextFileW(t_strFileNameW);
//				PrintWriter t_pwMSList    = FileIOUtils.openTextFileW(t_strFileNameMS);
//				PrintWriter t_pwLog       = FileIOUtils.openTextFileW(t_strFileNameLog);
//				GlycoCTtoWURCSFromFile(t_strFileNameR, t_strFileNameW, t_strFileNameLog);

				WURCSConversionLogger t_oLogger = new WURCSConversionLogger();
				TreeSet<String> t_setUniqueMS = new TreeSet<String>();
				TreeMap<String, String> t_mapIDToWURCS =
					GlycoCTListToWURCS(GlycoCTListReader.readFromFile(t_strFileNameR), t_setUniqueMS, t_oLogger);

				// Print result
				WURCSFileWriter.printWURCSList(t_mapIDToWURCS, t_strFilePathResult, t_strFileNameW);
				WURCSFileWriter.printMSList(t_setUniqueMS, t_strFilePathResult, t_strFileNameMS);
				t_oLogger.setInputFileName(t_strFileNameR);
				t_oLogger.printLog( WURCSFileWriter.getResultFilePath(t_strFilePathResult, t_strFileNameLog) );
				System.out.println("Output result to "+t_strFilePathResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** Convert and output WURCS from GlycoCT list */
	public static TreeMap<String, String> GlycoCTListToWURCS(TreeMap<String, String> a_mapGlycoCTList, TreeSet<String> a_setUniqueMS, WURCSConversionLogger a_oLogger) {

		TreeMap<String, String> t_mapIDToWURCS = new TreeMap<String, String>();

		for ( String ID : a_mapGlycoCTList.keySet() ) {

			String t_strCode = a_mapGlycoCTList.get(ID);
//			System.out.println(t_strCode);
			String t_strWURCS = "";
			String t_strMessage = "";
			WURCSExporterGlycoCT t_oExporter = new WURCSExporterGlycoCT();
//			StringBuffer t_strValidateGlycoCT = new StringBuffer("");
			try {
				// Start conversion
				/*
				SugarImporter t_oImporterGlycoCT = new SugarImporterGlycoCTCondensed();
				Sugar gx = t_oImporterGlycoCT.parse(t_strCode);
				gx = normalizeSugar(gx, t_strValidateGlycoCT);

				SugarToWURCSGraph t_oS2G = new SugarToWURCSGraph();
				t_oS2G.start(gx);
				WURCSGraph t_oGraph = t_oS2G.getGraph();

				// Get WURCS string
				WURCSFactory t_oFactory = new WURCSFactory(t_oGraph);
				t_strWURCS = t_oFactory.getWURCS();
				*/
				System.out.println(ID);
				t_oExporter.start(t_strCode);
				t_strWURCS = t_oExporter.getWURCS();

//				System.out.println( ID + "\t" + t_strWURCS );
//				String t_strWURCS = m_objSugarToWURCSGraph.getWURCSCompress();
				if( t_strWURCS.contains("WURCS") ) {
					// Collect unique monosaccharide
					WURCSGraphExporterUniqueMonosaccharides t_oExportMS = new WURCSGraphExporterUniqueMonosaccharides();
					WURCSFactory t_oFactory = new WURCSFactory(t_strWURCS);
					t_oExportMS.start( t_oFactory.getGraph() );
					for ( String t_strUniqueMS : t_oExportMS.getMSStrings() ) {
						if ( a_setUniqueMS.contains(t_strUniqueMS) ) continue;
						a_setUniqueMS.add(t_strUniqueMS);
					}
				}
//				System.out.println(ID);

			} catch ( SugarImporterException e ) {
				t_strMessage = "There is an error in GlycoCT importer";
				if ( e.getErrorText() != null ) {
					t_strMessage = e.getErrorText();
				}
			} catch ( GlycoVisitorException e ) {
				t_strMessage = e.getErrorMessage();
			} catch (WURCSException e) {
				t_strMessage = e.getErrorMessage();
			}

			if( t_strWURCS.contains("WURCS") ) {
				// Set result
				t_mapIDToWURCS.put(ID, t_strWURCS);
				a_oLogger.addWURCS(ID, t_strWURCS);
//				continue;
			}
			a_oLogger.addMessage(ID, t_strMessage, t_oExporter.getValidationErrorLog().toString());
		}

		return t_mapIDToWURCS;
	}


}
