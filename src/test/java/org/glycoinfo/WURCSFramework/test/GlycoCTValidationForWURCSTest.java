package org.glycoinfo.WURCSFramework.test;

import junit.framework.TestCase;

import org.eurocarbdb.MolecularFramework.io.SugarImporter;
import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarImporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.glycoinfo.WURCSFramework.util.GlycoVisitorValidationForWURCS;

public class GlycoCTValidationForWURCSTest extends TestCase {

	public void testWarning() {
		SugarImporter t_objImporterGlycoCT = new SugarImporterGlycoCTCondensed();
		GlycoVisitorValidationForWURCS t_oVal = new GlycoVisitorValidationForWURCS();

		String t_strWarning1 = "Anomeric symbol and ring size are not match.";
		String t_strWarning2 = "There is no anomeric position in the monosaccharide. It should be open chain.";

		String t_strCase1_1 = "x-dglc-HEX-0:0"; // o-dglc-HEX-0:0
		String t_strCase1_2 = "o-dglc-HEX-x:x"; // o-dglc-HEX-0:0
		String t_strCase2 = "x-dglc-HEX-x:x|1:aldi"; // o-dglc-HEX-0:0|1:aldi

		String t_strCode = "RES"
				+ "\n1b:"+t_strCase1_1
				+ "\n2b:"+t_strCase1_2
				+ "\n3b:"+t_strCase2;
		try {
			t_oVal.start( t_objImporterGlycoCT.parse(t_strCode) );
			assertTrue( t_oVal.getWarnings().contains( t_strWarning1+" :"+t_strCase1_1 ) );
			assertTrue( t_oVal.getWarnings().contains( t_strWarning1+" :"+t_strCase1_2 ) );
			assertTrue( t_oVal.getWarnings().contains( t_strWarning2+" :"+t_strCase2 ) );
		} catch ( SugarImporterException e ) {
		} catch ( GlycoVisitorException e ) {
		}
	}

	public void testError() {
		SugarImporter t_objImporterGlycoCT = new SugarImporterGlycoCTCondensed();
		GlycoVisitorValidationForWURCS t_oVal = new GlycoVisitorValidationForWURCS();

		String t_strError1 = "Modification \"aldi\" must be set to the C1.";
		String t_strError2 = "Can not do carboxylation to non-terminal carbon.";
		String t_strError3 = "Carboxylic acid contained in the ring. It must be use a substituent \"lactone\".";
		String t_strError4 = "Can not be deoxy to aldehyde.";

		String t_strCase1 = "o-dglc-HEX-0:0|2:aldi"; // o-dglc-HEX-0:0|1:aldi
		String t_strCase2 = "b-dglc-HEX-1:5|3:a"; // b-dglc-HEX-1:5|6:a
		String t_strCase3 = "b-dglc-HEX-1:5|1:a"; // o-dglc-HEX-0:0|1:a
		String t_strCase4 = "x-dglc-HEX-x:x|1:d"; // x-dglc-HEX-x:x|2:d

		String t_strCode = "RES"
				+ "\n1b:"+t_strCase1
				+ "\n2b:"+t_strCase2
				+ "\n3b:"+t_strCase3
				+ "\n4b:"+t_strCase4;
		try {
			t_oVal.start( t_objImporterGlycoCT.parse(t_strCode) );
			assertTrue( t_oVal.getErrors().contains( t_strError1+" :"+t_strCase1 ) );
			assertTrue( t_oVal.getErrors().contains( t_strError2+" :"+t_strCase2 ) );
			assertTrue( t_oVal.getErrors().contains( t_strError3+" :"+t_strCase3 ) );
			assertTrue( t_oVal.getErrors().contains( t_strError4+" :"+t_strCase4 ) );
		} catch ( SugarImporterException e ) {
		} catch ( GlycoVisitorException e ) {
		}

		String t_strError5 = "Glycosidic linkage must not be aldehyde or ketone position.";

		String t_strCase5 = "o-dglc-HEX-0:0"; // o-dglc-HEX-0:0|1:aldi
		t_strCode = "RES"
				+ "\n1b:b-dglc-HEX-1:5"
				+ "\n2b:"+t_strCase5
				+ "\nLIN"
				+ "\n1:1o(6+1)2d";
		try {
			t_oVal.start( t_objImporterGlycoCT.parse(t_strCode) );
			assertTrue( t_oVal.getErrors().contains( t_strError5+" :"+t_strCase5 ) );
		} catch ( SugarImporterException e ) {
		} catch ( GlycoVisitorException e ) {
		}

		String t_strError6 = "Ring start must be anomeric position.";

		String t_strCase6_1 = "x-dgro-dgal-NON-1:5|1:a|2:keto|3:d"; // x-dgro-dgal-NON-2:6|1:a|2:keto|3:d
		String t_strCase6_2 = "a-dxyl-HEX-1:5|4:keto|6:d"; // a-dxyl-HEX-1:5|1:keto|4:keto|6:d

		t_strCode = "RES"
				+ "\n1b:"+t_strCase6_1
				+ "\n2b:"+t_strCase6_2;
		try {
			t_oVal.start( t_objImporterGlycoCT.parse(t_strCode) );
			assertTrue( t_oVal.getErrors().contains( t_strError6+" :"+t_strCase6_1 ) );
			assertTrue( t_oVal.getErrors().contains( t_strError6+" :"+t_strCase6_2 ) );
		} catch ( SugarImporterException e ) {
		} catch ( GlycoVisitorException e ) {
		}
}
}
